using BooksMicroservice.Database;

namespace BooksMicroservice.Models.Authors {
    /// <summary>
    /// Модель представления списка авторов
    /// </summary>
    public class AuthorListViewModel {
        /// <summary>
        /// Список авторов
        /// </summary>
        public IEnumerable<Author> Authors { get; set; }
    }
}