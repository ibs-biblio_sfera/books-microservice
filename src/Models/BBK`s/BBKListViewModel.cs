using BooksMicroservice.Database;

namespace BooksMicroservice.Models.BBKS {
    /// <summary>
    /// Модель представления списка индексов ББК
    /// </summary>
    public class BBKListViewModel {
        /// <summary>
        /// Список индексов ББК
        /// </summary>
        public required IEnumerable<BBK> BBKS {get; set;}
    }
}