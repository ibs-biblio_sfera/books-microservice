using BooksMicroservice.Database;

namespace BooksMicroservice.Models.UDKS {
    /// <summary>
    /// Модель представления списка данных УДК
    /// </summary>
    public class UdkListViewModel {
        /// <summary>
        /// Список УДК
        /// </summary>
        public required IEnumerable<UDK> UDKS { get; set; }
    }
}