using Microsoft.EntityFrameworkCore;

namespace BooksMicroservice.Database {
    /// <summary>
    /// Контекст базы данных приложения
    /// </summary>
    public class ApplicationDbContext : DbContext
    {
        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="options">Парметры контекста БД</param>
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options) 
            {
            }
            /// <summary>
            /// Автор книги
            /// </summary>
            public DbSet<Author> Authors { get; set; }
            /// <summary>
            /// Книги
            /// </summary>
            public DbSet<Book> Books { get; set; }
            /// <summary>
            /// Жанры книг
            /// </summary>
            public DbSet<Genre> Genres { get; set; }
            /// <summary>
            /// Справочник ББК
            /// </summary>
            public DbSet<BBK> BBKS { get; set; }
            /// <summary>
            /// Справочник УДК
            /// </summary>
            public DbSet<UDK> UDKS { get; set; }
    }
}