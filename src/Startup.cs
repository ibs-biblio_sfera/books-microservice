using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using System.Text;
using BooksMicroservice.Database;
using System.Reflection;
using StackExchange.Redis;

namespace BooksMicroservice {

    /// <summary>
    /// Описание конфигурации приложения
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Инициализирует новый экземпляр класса Startup с указанной конфигурацией.
        /// </summary>
        /// <param name="configuration">Объект, представляющий интерфейс IConfiguration, содержащий конфигурационные данные приложения.</param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        /// <summary>
        /// Конфигурация приложения.
        /// </summary>
        public IConfiguration Configuration { get; }
        
        /// <summary>
        /// Настраивает службы, необходимые для работы приложения.
        /// </summary>
        /// <param name="services">Коллекция служб, которые будут настроены.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            ConfigureDataBase(services);
            ConfigureSwagger(services);
            ConfigureCache(services);
            services.AddMediatR(cfg => cfg.RegisterServicesFromAssemblies(Assembly.GetExecutingAssembly()));
            services.AddHealthChecks();
            services.AddControllers();
            
        }

        private void ConfigureCache(IServiceCollection services) {
            services.AddStackExchangeRedisCache(options =>
            {
                options.Configuration = Environment.GetEnvironmentVariable("REDIS_URL") ?? "localhost:6379";
                options.InstanceName = Environment.GetEnvironmentVariable("INSTANCE_NAME") ??"DevBooksMicroservice";
            });
            services.AddSingleton<ConnectionMultiplexer>(sp =>
            {
                var configuration = Environment.GetEnvironmentVariable("REDIS_URL") ?? "localhost:6379";
                return ConnectionMultiplexer.Connect(configuration);
            });
        }

        private void ConfigureDataBase(IServiceCollection services) {
            string? connectionString = Environment.GetEnvironmentVariable("DATABASE_URL") ?? Configuration.GetConnectionString("PostgresConnection");
            services.AddDbContext<ApplicationDbContext>(options => options.UseNpgsql(connectionString));
        }

        private void ConfigureSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "IBS-BooksMicroservice Api", Version = "v1" });
            });
        }

        /// <summary>
        /// Конфигурирует приложение для обработки запросов и управления их маршрутизацией.
        /// </summary>
        /// <param name="app">Объект, представляющий построитель приложения ASP.NET Core.</param>
        /// <param name="env">Объект, представляющий среду выполнения веб-приложения.</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseSwagger();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/error");
                app.UseHsts();
            }
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Kegostrov Library BooksMicroservice Api v1");
            });
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHealthChecks("/health");
                endpoints.MapControllers();
            });
        }
    }

}