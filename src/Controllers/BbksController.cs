using Microsoft.AspNetCore.Mvc;
using MediatR;
using BooksMicroservice.Features.BBKS;
using BooksMicroservice.Models.BBKS;
using BooksMicroservice.Database;

namespace BooksMicroservice.Controllers {
    /// <summary>
    /// Api контроллер для работы с индексом ББК
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class BbksController(IMediator mediator) : Controller {
        private readonly IMediator _mediator = mediator;

        /// <summary>
        /// Контроллер отправляет список ББК индексов
        /// </summary>
        /// <param name="query">Параметры запроса</param>
        /// <returns>Список ББК игндексов</returns>
        [HttpGet]
        public async Task<ActionResult<BBKListViewModel>> Get([FromQuery] GetBBKSQuery query) 
        {
            var result = await _mediator.Send(query);
            return Ok(result);
        }

        /// <summary>
        /// Контроллер отправляет индекс ББК
        /// </summary>
        /// <param name="id">Уникальный идентификатор ББК</param>
        /// <returns>ББК индекс</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<BBK>> GetById(Guid id) 
        {
            var query = new GetBbkByIdQuery {BbkId = id};
            var result = await _mediator.Send(query);

            if (result == null)
            {
                return NotFound();
            }

            return result;

        }

        /// <summary>
        /// Метод создает новый индекс ББК в базе данных.
        /// </summary>
        /// <param name="command">Модель данных для создания ББК</param>
        /// <returns>Созданный индекс ББК</returns>
        [HttpPost]
        public async Task<ActionResult<BBK>> AddBbk(CreateBbkCommand command)
        {
            try {
                var result = await _mediator.Send(command);
                return CreatedAtAction(nameof(GetById), new { id = result.BbkId }, result);
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return StatusCode(400, ex.Message);
            }
            
        }

        /// <summary>
        /// Метод обновляет данные о ББК.
        /// </summary>
        /// <param name="id">Идентификатор БК</param>
        /// <param name="command">Модель данных ББК</param>
        /// <returns>Модель представления ББК</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateBbk(Guid id, UpdateBbkCommand command)
        {
            if (id != command.BbkId)
            {
                return BadRequest("ОШИБКА: Идентификатор ББК не совпадает с передаными данными");
            }
            try {
                command.BbkId = id;
                var result = await _mediator.Send(command);
                return CreatedAtAction(nameof(GetById), new { id = result.BbkId }, result);
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return BadRequest(ex.Message);
            }
        }
        /// <summary>
        /// Метод частично обновляет данные о ББК.
        /// </summary>
        /// <param name="id">Идентификатор ББК</param>
        /// <param name="command">Модель данных ББК</param>
        /// <returns>Представление ББК</returns>
        [HttpPatch("{id}")]
        public async Task<IActionResult> PatchBbk(Guid id, UpdateBbkCommand command) {
            if (id != command.BbkId)
            {
                return BadRequest("ОШИБКА: Идентификатор ББК не совпадает с передаными данными");
            }
            try {
                command.BbkId = id;
                var result = await _mediator.Send(command);
                return CreatedAtAction(nameof(GetById), new { id = result.BbkId }, result);
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Метод удаляет ББК из базы данных.
        /// </summary>
        /// <param name="id">Идентификатор ББК</param>
        /// <returns>201 - если ББК удалена, 400 - если ошибка</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBbk(Guid id)
        {
            try {
                await _mediator.Send(new DeleteBbkCommand { BbkId = id });
                return NoContent();
            }
            catch (Exception ex) {
                return BadRequest(ex.Message);
            }
        } 
        
    }
}