using Microsoft.AspNetCore.Mvc;
using MediatR;
using BooksMicroservice.Features.UDKS;
using BooksMicroservice.Models.UDKS;
using BooksMicroservice.Database;

namespace BooksMicroservice.Controllers {
    /// <summary>
    /// Api контроллер для работы с индексом УДК
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class UdksController(IMediator mediator) : Controller {
        private readonly IMediator _mediator = mediator;

        /// <summary>
        /// Контроллер отправляет список УДК индексов
        /// </summary>
        /// <param name="query">Параметры запроса</param>
        /// <returns>Список УДК игндексов</returns>
        [HttpGet]
        public async Task<ActionResult<UdkListViewModel>> Get([FromQuery] GetUdkQuery query) 
        {
            var result = await _mediator.Send(query);
            return Ok(result);
        }

        /// <summary>
        /// Контроллер отправляет индекс УДК
        /// </summary>
        /// <param name="id">Уникальный идентификатор УДК</param>
        /// <returns>УДК индекс</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<UDK>> GetById(Guid id) 
        {
            var query = new GetUdkByIdQuery {UdkId = id};
            var result = await _mediator.Send(query);

            if (result == null)
            {
                return NotFound();
            }

            return result;

        }

        /// <summary>
        /// Метод создает новый индекс УДК в базе данных.
        /// </summary>
        /// <param name="command">Модель данных для создания УДК</param>
        /// <returns>Созданный индекс УДК</returns>
        [HttpPost]
        public async Task<ActionResult<UDK>> AddUdk(CreateUdkCommand command)
        {
            try {
                var result = await _mediator.Send(command);
                return CreatedAtAction(nameof(GetById), new { id = result.UdkId }, result);
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return StatusCode(400, ex.Message);
            }
            
        }

        /// <summary>
        /// Метод обновляет данные о УДК.
        /// </summary>
        /// <param name="id">Идентификатор УДК</param>
        /// <param name="command">Модель данных УДК</param>
        /// <returns>Модель представления УДК</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateUdk(Guid id, UpdateUdkCommand command)
        {
            if (id != command.UdkId)
            {
                return BadRequest("ОШИБКА: Идентификатор ББК не совпадает с передаными данными");
            }
            try {
                command.UdkId = id;
                var result = await _mediator.Send(command);
                return CreatedAtAction(nameof(GetById), new { id = result.UdkId }, result);
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return BadRequest(ex.Message);
            }
        }
        /// <summary>
        /// Метод частично обновляет данные о УДК.
        /// </summary>
        /// <param name="id">Идентификатор УДК</param>
        /// <param name="command">Модель данных УДК</param>
        /// <returns>Представление УДК</returns>
        [HttpPatch("{id}")]
        public async Task<IActionResult> PatchUdk(Guid id, UpdateUdkCommand command) {
            if (id != command.UdkId)
            {
                return BadRequest("ОШИБКА: Идентификатор ББК не совпадает с передаными данными");
            }
            try {
                command.UdkId = id;
                var result = await _mediator.Send(command);
                return CreatedAtAction(nameof(GetById), new { id = result.UdkId }, result);
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Метод удаляет УДК из базы данных.
        /// </summary>
        /// <param name="id">Идентификатор УДК</param>
        /// <returns>201 - если УДК удалена, 400 - если ошибка</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBbk(Guid id)
        {
            try {
                await _mediator.Send(new DeleteUdkCommand { UdkId = id });
                return NoContent();
            }
            catch (Exception ex) {
                return BadRequest(ex.Message);
            }
        } 
        
    }
}