using BooksMicroservice.Database;
using BooksMicroservice.Features.Authors;
using BooksMicroservice.Models.Authors;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace BooksMicroservice.Controllers {

    /// <summary>
    /// Контроллер для работы с автором
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class AuthorsController : ControllerBase
    {
        private readonly IMediator _mediator;
        
        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="mediator">экземпляр класса MediaTr</param>
        public AuthorsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Метод находит авторов в базе данных.
        /// </summary>
        /// <param name="query">Параметры запроса</param>
        /// <returns>Список авторов</returns>
        [HttpGet]
        public async Task<ActionResult<AuthorListViewModel>> GetBooks([FromQuery] GetAuthorQuery query)
        {
            var result = await _mediator.Send(query);
            return Ok(result);
        }

        /// <summary>
        /// Контроллер находит автора в базе данных по идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор автора</param>
        /// <returns>Модель представления автора</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(200, Type = typeof(Author))]
        [ProducesResponseType(404)]
        public async Task<ActionResult<Author>> GetAuthorById(Guid id)
        {
            var query = new GetAuthorByIdQuery { AuthorId = id };
            var result = await _mediator.Send(query);

            if (result == null)
            {
                return NotFound();
            }

            return result;
        }

        /// <summary>
        /// Метод создает нового автора в базе данных.
        /// </summary>
        /// <param name="command">Модель данных для создания автора</param>
        /// <returns>Модель представления автора</returns>
        [HttpPost]
        public async Task<ActionResult<Author>> AddBook(CreateAuthorCommand command)
        {
            try {
                var result = await _mediator.Send(command);
                return CreatedAtAction(nameof(GetAuthorById), new { id = result.AuthorId }, result);
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return StatusCode(400, ex.Message);
            }
            
        }

        /// <summary>
        /// Метод обновляет данные об авторе.
        /// </summary>
        /// <param name="id">Идентификатор автора</param>
        /// <param name="command">Модель данных автора</param>
        /// <returns>Модель представления автора</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateAuthor(Guid id, UpdateAuthorCommand command)
        {
            if (id != command.AuthorId)
            {
                return BadRequest("ОШИБКА: Идентификатор автора не совпадает с передаными данными");
            }
            try {
                command.AuthorId = id;
                var result = await _mediator.Send(command);
                return CreatedAtAction(nameof(GetAuthorById), new { id = result.AuthorId }, result);
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return BadRequest(ex.Message);
            }
        }
        /// <summary>
        /// Метод частично обновляет данные об авторе.
        /// </summary>
        /// <param name="id">Идентификатор автора</param>
        /// <param name="command">Модель данных автора</param>
        /// <returns>Модель представления автора</returns>
        [HttpPatch("{id}")]
        public async Task<IActionResult> PatchAuthor(Guid id, UpdateAuthorCommand command) {
            if (id != command.AuthorId)
            {
                return BadRequest("ОШИБКА: Идентификатор автора не совпадает с передаными данными");
            }
            try {
                command.AuthorId = id;
                var result = await _mediator.Send(command);
                return CreatedAtAction(nameof(GetAuthorById), new { id = result.AuthorId }, result);
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Метод удаляет автора из базы данных.
        /// </summary>
        /// <param name="id">Идентификатор автора</param>
        /// <returns>201 - если автор удален, 400 - если ошибка</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAuthor(Guid id)
        {
            try {
                await _mediator.Send(new DeleteAuthorCommand { AuthorId = id });
                return NoContent();
            }
            catch (Exception ex) {
                return BadRequest(ex.Message);
            }
        }     
    }
}