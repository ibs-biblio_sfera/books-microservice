using BooksMicroservice.Database;
using MediatR;

namespace BooksMicroservice.Features.Authors
{
    /// <summary>
    /// Модель данных для обновления автора
    /// </summary>
    public class UpdateAuthorCommand : IRequest<Author> {

        /// <summary>
        /// Идентификатор автора
        /// </summary>
        public Guid AuthorId { get; set; }
        /// <summary>
        /// Полное имя автора
        /// </summary>
        public string? LongName {get; set;}
        /// <summary>
        /// Короткое имя автора
        /// </summary>
        public string? ShortName { get; set; }
    }
}