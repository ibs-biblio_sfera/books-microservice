using BooksMicroservice.Database;
using MediatR;
using StackExchange.Redis;

namespace BooksMicroservice.Features.Authors {
    /// <summary>
    /// Модуль обновления Автора
    /// </summary>
    public class UpdateAuthorHandler : IRequestHandler<UpdateAuthorCommand, Author> {
        private readonly ApplicationDbContext _context;
        private readonly IDatabase _redis;
        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="context">Контекст базы данных</param>
        /// <param name="redis">Контекст Redis</param>

        public UpdateAuthorHandler(ApplicationDbContext context, ConnectionMultiplexer redis) {
            _context = context;
            _redis = redis.GetDatabase();
        }
        
        /// <summary>
        /// Метод обновляет Автора в БД
        /// </summary>
        /// <param name="request">Модель представления данных</param>
        /// <param name="cancellationToken"></param>
        /// <returns>Модель представления автора</returns>
        /// <exception cref="Exception">Вызывается если такой Автор не существует</exception>
        public async Task<Author> Handle(UpdateAuthorCommand request, CancellationToken cancellationToken)  {
            var cacheKey = $"udk:{request.AuthorId}";
            var currentAuthor = await _context.Authors.FindAsync(request.AuthorId, cancellationToken);
            if (currentAuthor == null) {
                throw new Exception($"ОШИБКА: Не удалось найти Автора с ID {request.AuthorId}");
            }
            currentAuthor.LongName = request.LongName ?? currentAuthor.LongName;
            currentAuthor.ShortName = request.ShortName ?? currentAuthor.ShortName;
            await _redis.KeyDeleteAsync(cacheKey);
            await _context.SaveChangesAsync(cancellationToken);
            return currentAuthor;
        }
    }
}