using BooksMicroservice.Database;
using MediatR;

namespace BooksMicroservice.Features.Authors {
    /// <summary>
    /// Модель параметра запроса на получения индекса Автора по его ID
    /// </summary>
    public class GetAuthorByIdQuery : IRequest<Author> {
        /// <summary>
        /// Уникальный идентификатор Автора
        /// </summary>
        public required Guid AuthorId { get; set; }
    }
}