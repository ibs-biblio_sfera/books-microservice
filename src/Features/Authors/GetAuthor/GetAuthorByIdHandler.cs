using BooksMicroservice.Database;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace BooksMicroservice.Features.Authors {
    /// <summary>
    /// Модуль ищет данные Автора по его ID
    /// </summary>
    /// <param name="context">Контекст базы данных</param>
    /// <param name="redis">База данных редис</param>
    public class GetUdkByIdHandler(ApplicationDbContext context, ConnectionMultiplexer redis) : IRequestHandler<GetAuthorByIdQuery, Author> {
        private readonly ApplicationDbContext _context = context;
        private readonly ConnectionMultiplexer _redis = redis;

        public async Task<Author> Handle(GetAuthorByIdQuery request, CancellationToken cancellationToken) {
            var cacheKey = $"author:{request.AuthorId}";
            var redisDb = _redis.GetDatabase();
            var cachedAuthor = await redisDb.StringGetAsync(cacheKey);
            if (!cachedAuthor.IsNullOrEmpty)
            {
                return JsonConvert.DeserializeObject<Author>(cachedAuthor);
            }
            var response = await _context.Authors.FirstOrDefaultAsync(x => x.AuthorId == request.AuthorId, cancellationToken);
            if (response != null)
            {
                await redisDb.StringSetAsync(cacheKey, JsonConvert.SerializeObject(response), TimeSpan.FromMinutes(10));
            }
            return response;
        }
    }
}