using MediatR;
using BooksMicroservice.Database;
using Microsoft.EntityFrameworkCore;
using BooksMicroservice.Models.Authors;

namespace BooksMicroservice.Features.Authors {
    /// <summary>
    /// Моудль для получения списка Автора
    /// </summary>
    /// <param name="context">Контекст базы данных</param>
    public class GetAuthorQueryHandler(ApplicationDbContext context) : IRequestHandler<GetAuthorQuery, AuthorListViewModel> {
        private readonly ApplicationDbContext _context = context;

        /// <summary>
        /// Метод для получения списка Авторов
        /// </summary>
        /// <param name="request">Параметры запроса</param>
        /// <param name="cancellationToken"></param>
        /// <returns>Список Авторов</returns>
        public async Task<AuthorListViewModel> Handle(GetAuthorQuery request, CancellationToken cancellationToken) {
            var authors = await _context.Authors.ToListAsync(cancellationToken);
            return new AuthorListViewModel {
                Authors = authors
            };
        }
    }
}