using BooksMicroservice.Models.Authors;
using MediatR;

namespace BooksMicroservice.Features.Authors {
    public class GetAuthorQuery : IRequest<AuthorListViewModel> {
        
    }
}