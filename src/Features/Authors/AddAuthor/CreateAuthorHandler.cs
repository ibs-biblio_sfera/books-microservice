using BooksMicroservice.Database;
using MediatR;

namespace BooksMicroservice.Features.Authors {
    /// <summary>
    /// Модуль для создания данных об авторе
    /// </summary>
    /// <param name="context">Контекст базы данных</param>
    public class CreateAuthorHandler(ApplicationDbContext context) : IRequestHandler<CreateAuthorCommand, Author> {
        private readonly ApplicationDbContext _context = context;

        /// <summary>
        /// Метод создает индекс УДК в базе данных
        /// </summary>
        /// <param name="request">Модель данных для создания</param>
        /// <param name="cancellationToken"></param>
        /// <returns>Созданный индекс УДК</returns>
        public async Task<Author> Handle(CreateAuthorCommand request, CancellationToken cancellationToken) {
            var newAuthor = new Author {
                LongName = request.LongName,
                ShortName = request.ShortName
            };
            await _context.Authors.AddAsync(newAuthor, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);
            return newAuthor;
        }

    }
}