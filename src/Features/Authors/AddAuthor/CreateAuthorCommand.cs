using BooksMicroservice.Database;
using MediatR;

namespace BooksMicroservice.Features.Authors {
    /// <summary>
    /// Модель данных для автора в БД
    /// </summary>
    public class CreateAuthorCommand : IRequest<Author> {
        /// <summary>
        /// Полное имя автора
        /// </summary>
        public required string LongName {get; set;}
        /// <summary>
        /// Короткое имя автора
        /// </summary>
        public required string ShortName {get; set; }
    }
}