using MediatR;

namespace BooksMicroservice.Features.Authors {
    /// <summary>
    /// Модель данных для осуществления удаления Автора из БД
    /// </summary>
    public class DeleteAuthorCommand : IRequest<Unit> {
        /// <summary>
        /// Уникальный идентификатор Автора
        /// </summary>
        public Guid AuthorId { get; set; }
    }
}