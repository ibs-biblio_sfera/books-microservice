using BooksMicroservice.Database;
using MediatR;
using StackExchange.Redis;

namespace BooksMicroservice.Features.Authors {
    /// <summary>
    /// Класс удаления авторов
    /// </summary>
    /// <param name="context">Контекст базы данных</param>
    public class DeleteAuthorHadler(ApplicationDbContext context, ConnectionMultiplexer redis) : IRequestHandler<DeleteAuthorCommand, Unit> {
        private readonly ApplicationDbContext _context = context;
        private readonly IDatabase _redis = redis.GetDatabase();
        /// <summary>
        /// Метод находит Автора в базе и удаляет его
        /// </summary>
        /// <param name="request">Идентфикатор автора</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="Exception">Вызывается если Автор не был найден</exception>
        public async Task<Unit> Handle(DeleteAuthorCommand request, CancellationToken cancellationToken) {
            var cacheKey = $"author:{request.AuthorId}";
            var author = await _context.Authors.FindAsync(request.AuthorId);
            if (author == null) {
                throw new Exception($"ОШИБКА: Не удалось найти Автора с ID {request.AuthorId}");
            }
            _context.Authors.Remove(author);
            await _context.SaveChangesAsync(cancellationToken);
            await _redis.KeyDeleteAsync(cacheKey);
            return Unit.Value;
        }
    }
}