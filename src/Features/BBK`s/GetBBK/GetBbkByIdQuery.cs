using BooksMicroservice.Database;
using MediatR;

namespace BooksMicroservice.Features.BBKS {
    /// <summary>
    /// Модель представления данных для получения индекса ББК
    /// </summary>
    public class GetBbkByIdQuery : IRequest<BBK> {
        /// <summary>
        /// Уникальный идентификатор индекса ББК
        /// </summary>
        public required Guid BbkId { get; set; }
    }
}