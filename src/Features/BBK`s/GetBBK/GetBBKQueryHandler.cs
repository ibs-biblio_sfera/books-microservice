using MediatR;
using BooksMicroservice.Models.BBKS;
using BooksMicroservice.Database;
using Microsoft.EntityFrameworkCore;
using StackExchange.Redis;

namespace BooksMicroservice.Features.BBKS {
    /// <summary>
    /// Класс получения списка книг
    /// </summary>
    /// <param name="context">Контекст базы данных</param>
    public class GetBbkQueryHandler(ApplicationDbContext context) : IRequestHandler<GetBBKSQuery, BBKListViewModel> {
        private readonly ApplicationDbContext _context = context;
        /// <summary>
        /// Метод выводит список индексов ББК
        /// </summary>
        /// <param name="request">Параметры запроса</param>
        /// <param name="cancellationToken"></param>
        /// <returns>Список индексов ББК</returns>
        public async Task<BBKListViewModel> Handle(GetBBKSQuery request, CancellationToken cancellationToken) {
            var result = await _context.BBKS.ToListAsync();
            return new BBKListViewModel {
                            BBKS = result
                        };
        }
    }
}