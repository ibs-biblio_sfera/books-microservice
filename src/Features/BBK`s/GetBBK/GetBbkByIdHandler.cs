using BooksMicroservice.Database;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using StackExchange.Redis;
namespace BooksMicroservice.Features.BBKS {
    /// <summary>
    /// Модуль получения индекса ББК по id
    /// </summary>
    /// <param name="context">Контекст базы данных</param>
    /// <param name="redis">База кэширования</param>
    public class GetBbkByIdQueryHandler(ApplicationDbContext context, ConnectionMultiplexer redis) : IRequestHandler<GetBbkByIdQuery, BBK> {
        private readonly ApplicationDbContext _context = context;
        private readonly ConnectionMultiplexer _redis = redis;
        /// <summary>
        /// Метод получает индекс ББК по id
        /// </summary>
        /// <param name="request">Идентификатор ББК</param>
        /// <param name="cancellationToken"></param>
        /// <returns>Индекс ББк</returns>
        public async Task<BBK> Handle(GetBbkByIdQuery request, CancellationToken cancellationToken) {
            var cacheKey = $"bbk:{request.BbkId}";
            var redisDb = _redis.GetDatabase();
            var cachedBbk = await redisDb.StringGetAsync(cacheKey);
            if (!cachedBbk.IsNullOrEmpty)
            {
                return JsonConvert.DeserializeObject<BBK>(cachedBbk);
            }
            var response = await _context.BBKS.FirstOrDefaultAsync(x => x.BbkId == request.BbkId, cancellationToken);
            if (response != null)
            {
                await redisDb.StringSetAsync(cacheKey, JsonConvert.SerializeObject(response), TimeSpan.FromMinutes(10));
            }
            return response;
        }
    }
}