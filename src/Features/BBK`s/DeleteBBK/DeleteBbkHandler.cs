using BooksMicroservice.Database;
using MediatR;
using StackExchange.Redis;

namespace BooksMicroservice.Features.BBKS {
    public class DeleteBbkHandler : IRequestHandler<DeleteBbkCommand, Unit>
    {
        private readonly ApplicationDbContext _context;
        private readonly IDatabase _redis;

        public DeleteBbkHandler(ApplicationDbContext context, ConnectionMultiplexer redis)
        {
            _context = context;
            _redis = redis.GetDatabase();
        }

        /// <summary>
        /// Метод находит ББК в базе и удаляет его
        /// </summary>
        /// <param name="request">Команда с id ББК</param>
        /// <param name="cancellationToken"></param>
        /// <returns>Task, представляющий асинхронную операцию</returns>
        /// <exception cref="Exception">Вызывается, если не найден id ББК</exception>
        public async Task<Unit> Handle(DeleteBbkCommand request, CancellationToken cancellationToken)
        {
            var cacheKey = $"bbk:{request.BbkId}";
            var bbk = await _context.BBKS.FindAsync(request.BbkId);

            if (bbk == null)
            {
                throw new Exception($"ОШИБКА: Не удалось найти индекс ББК с ID {request.BbkId}");
            }

            _context.BBKS.Remove(bbk);
            await _context.SaveChangesAsync();
            await _redis.KeyDeleteAsync(cacheKey);
            return Unit.Value;
        }
    }
}