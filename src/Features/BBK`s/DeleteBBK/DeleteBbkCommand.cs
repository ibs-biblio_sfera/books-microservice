using MediatR;

namespace BooksMicroservice.Features.BBKS {
    /// <summary>
    /// Модель для удаления данных ББК
    /// </summary>
    public class DeleteBbkCommand : IRequest<Unit> 
    {
        /// <summary>
        /// Идентификатор ББК
        /// </summary>
        public Guid BbkId { get; set; }
    }
}