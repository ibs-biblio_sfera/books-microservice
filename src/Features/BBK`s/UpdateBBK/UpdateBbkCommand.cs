using BooksMicroservice.Database;
using MediatR;
namespace BooksMicroservice.Features.BBKS {
    /// <summary>
    /// Модель обновления Индекса ББк
    /// </summary>
    public class UpdateBbkCommand : IRequest<BBK> {
        /// <summary>
        /// Уникальный идентификатор ББК
        /// </summary>
        public Guid BbkId { get; set; }
        /// <summary>
        /// Текстовое значение ББК
        /// </summary>
        public string? Type { get; set; }
        /// <summary>
        /// Код ББК
        /// </summary>
        public string? Code { get; set; }
    }
}