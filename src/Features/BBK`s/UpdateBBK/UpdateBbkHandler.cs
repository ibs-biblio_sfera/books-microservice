using BooksMicroservice.Database;
using MediatR;
using StackExchange.Redis;
namespace BooksMicroservice.Features.BBKS {
    /// <summary>
    /// Модуль обновления данных ббк
    /// </summary>
    /// <param name="context">Контекст базы данных</param>
    /// <param name="redis">Редис кэш</param>
    public class UpdateBbkHandler(ApplicationDbContext context, ConnectionMultiplexer redis) : IRequestHandler<UpdateBbkCommand, BBK> {
        private readonly ApplicationDbContext _context = context;
        private readonly IDatabase _redis = redis.GetDatabase();
        /// <summary>
        /// Метод обновляет данных ББК
        /// </summary>
        /// <param name="request">Модель данных</param>
        /// <param name="cancellationToken"></param>
        /// <returns>Обновленную запись</returns>
        /// <exception cref="Exception">Вызывается при отсутствии данных внутри БД</exception>
        public async Task<BBK> Handle(UpdateBbkCommand request, CancellationToken cancellationToken) {
            var cacheKey = $"bbk:{request.BbkId}";
            var cuurentBbk = await _context.BBKS.FindAsync(request.BbkId);
            if (cuurentBbk == null) {
                throw new Exception($"ОШИБКА: Не удалось найти ББК с ID {request.BbkId}");
            }
            cuurentBbk.Type = request.Type ?? cuurentBbk.Type;
            cuurentBbk.Code = request.Code ?? cuurentBbk.Code;
            await _context.SaveChangesAsync();
            await _redis.KeyDeleteAsync(cacheKey);
            return cuurentBbk;
        }
    }
}