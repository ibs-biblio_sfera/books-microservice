using BooksMicroservice.Database;
using MediatR;


namespace BooksMicroservice.Features.BBKS {
    /// <summary>
    /// Модель данных для создания ББК В БД
    /// </summary>
    public class CreateBbkCommand : IRequest<BBK> {
        /// <summary>
        /// Текстовое значение ББК
        /// </summary>
        public required string Type { get; set; }
        /// <summary>
        /// Код ББК
        /// </summary>
        public required string Code { get; set; }
    }
}