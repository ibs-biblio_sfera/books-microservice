using BooksMicroservice.Database;
using MediatR;

namespace BooksMicroservice.Features.BBKS {
    /// <summary>
    /// Модуль создания индекса ББК
    /// </summary>
    /// <param name="context"></param>
    public class CreateBbkhandler(ApplicationDbContext context) : IRequestHandler<CreateBbkCommand, BBK> {
        private readonly ApplicationDbContext _context = context;
        /// <summary>
        /// Метод создает индекс ББК в базе данных
        /// </summary>
        /// <param name="request">Модель данных для создания</param>
        /// <param name="cancellationToken"></param>
        /// <returns>Созданный индекс ББК</returns>
        public async Task<BBK> Handle(CreateBbkCommand request, CancellationToken cancellationToken) {
            var bbk = new BBK {
                Type = request.Type,
                Code = request.Code
            };
            _context.BBKS.Add(bbk);
            await _context.SaveChangesAsync(cancellationToken);
            return bbk;
        }
    }
}