using BooksMicroservice.Database;
using BooksMicroservice.Models.Books;
using MediatR;
using Microsoft.EntityFrameworkCore;
using StackExchange.Redis;
using Newtonsoft.Json;


namespace BooksMicroservice.Features.Books {
    /// <summary>
    /// Логика получения списка книг
    /// </summary>
    public class GetBooksQueryHandler : IRequestHandler<GetBooksQuery, BookListViewModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly ConnectionMultiplexer _redis;

        public GetBooksQueryHandler(ApplicationDbContext context, ConnectionMultiplexer redis)
        {
            _redis = redis;
            _context = context;
        }

        /// <summary>
        /// Метод находит книги в базе
        /// </summary>
        /// <param name="request">Номер страницы и колчество записей на одной странце</param>
        /// <param name="cancellationToken"></param>
        /// <returns>Список книг</returns>
        public async Task<BookListViewModel> Handle(GetBooksQuery request, CancellationToken cancellationToken)
        {
            var cacheKey = $"books:{request.Page}:{request.PageSize}";
            var cachedBooks = await _redis.GetDatabase().StringGetAsync(cacheKey);

            if (!cachedBooks.IsNullOrEmpty)
            {
                return JsonConvert.DeserializeObject<BookListViewModel>(cachedBooks);
            }

            var books = await _context.Books
                .Include(book => book.Genre)
                .Include(book => book.BBK)
                .Include(book => book.UDK)
                .Include(book => book.Author)
                .OrderByDescending(b => b.UploadDate)
                .Skip((request.Page - 1) * request.PageSize)
                .Take(request.PageSize)
                .ToListAsync();

            var viewModel = new BookListViewModel
            {
                Books = books
            };

            await _redis.GetDatabase().StringSetAsync(cacheKey, JsonConvert.SerializeObject(viewModel), TimeSpan.FromMinutes(15));

            return viewModel;
        }
    }
}