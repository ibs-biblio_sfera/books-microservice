using MediatR;

namespace BooksMicroservice.Features.Books {
    /// <summary>
    /// Команда с Id книги
    /// </summary>
    public class DeleteBookCommand : IRequest<Unit>
    {
        /// <summary>
        /// Идентификатор книги
        /// </summary>
        public Guid BookId { get; set; }
    }
}