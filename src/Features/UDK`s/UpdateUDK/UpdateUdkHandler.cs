using BooksMicroservice.Database;
using MediatR;
using StackExchange.Redis;

namespace BooksMicroservice.Features.UDKS {
    /// <summary>
    /// Модуль обновления индекса УДК
    /// </summary>
    public class UpdateUdkHandler : IRequestHandler<UpdateUdkCommand, UDK> {
        private readonly ApplicationDbContext _context;
        private readonly IDatabase _redis;
        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="context">Контекст базы данных</param>

        public UpdateUdkHandler(ApplicationDbContext context, ConnectionMultiplexer redis) {
            _context = context;
            _redis = redis.GetDatabase();
        }
        
        /// <summary>
        /// Метод обновляет жанр в БД
        /// </summary>
        /// <param name="request">Модель представления данных</param>
        /// <param name="cancellationToken"></param>
        /// <returns>Жанр</returns>
        /// <exception cref="Exception">Вызывается если такой жанр не существует</exception>
        public async Task<UDK> Handle(UpdateUdkCommand request, CancellationToken cancellationToken)  {
            var cacheKey = $"udk:{request.UdkId}";
            var currentUdk = await _context.UDKS.FindAsync(request.UdkId, cancellationToken);
            if (currentUdk == null) {
                throw new Exception($"ОШИБКА: Не удалось найти УДК с ID {request.UdkId}");
            }
            currentUdk.Code = request.Code ?? currentUdk.Code;
            currentUdk.Type = request.Type ?? currentUdk.Type;
            await _redis.KeyDeleteAsync(cacheKey);
            await _context.SaveChangesAsync(cancellationToken);
            return currentUdk;
        }
    }
}