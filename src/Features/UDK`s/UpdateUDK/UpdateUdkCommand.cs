using BooksMicroservice.Database;
using MediatR;

namespace BooksMicroservice.Features.UDKS 
{
    /// <summary>
    /// Модель данных для обновления индекса УДК
    /// </summary>
    public class UpdateUdkCommand : IRequest<UDK> {

        /// <summary>
        /// Идентификатор индекса УДК
        /// </summary>
        public Guid UdkId { get; set; }
        /// <summary>
        /// Название индекса УДК
        /// </summary>
        public string? Type { get; set; }
        /// <summary>
        /// Код УДК
        /// </summary>
        public string? Code { get; set; }
    }
}