using BooksMicroservice.Database;
using MediatR;

namespace BooksMicroservice.Features.UDKS {
    /// <summary>
    /// Модуль для создания данных Об УДК
    /// </summary>
    /// <param name="context">Контекст базы данных</param>
    public class CreateUdkHandler(ApplicationDbContext context) : IRequestHandler<CreateUdkCommand, UDK> {
        private readonly ApplicationDbContext _context = context;

        /// <summary>
        /// Метод создает индекс УДК в базе данных
        /// </summary>
        /// <param name="request">Модель данных для создания</param>
        /// <param name="cancellationToken"></param>
        /// <returns>Созданный индекс УДК</returns>
        public async Task<UDK> Handle(CreateUdkCommand request, CancellationToken cancellationToken) {
            var newUdk = new UDK {
                Type = request.Type,
                Code = request.Code
            };
            await _context.UDKS.AddAsync(newUdk, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);
            return newUdk;
        }

    }
}