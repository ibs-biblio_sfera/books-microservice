using BooksMicroservice.Database;
using MediatR;

namespace BooksMicroservice.Features.UDKS {
    /// <summary>
    /// Модель данных для создания индекса УДК в Базе
    /// </summary>
    public class CreateUdkCommand : IRequest<UDK> {
        /// <summary>
        /// Текстовая расшифровка УДК кода
        /// </summary>
        public required string Type { get; set; }
        /// <summary>
        /// Код по справочнику УДК
        /// </summary>
        public required string Code { get; set; }
    }
}