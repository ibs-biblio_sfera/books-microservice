using BooksMicroservice.Database;
using MediatR;

namespace BooksMicroservice.Features.UDKS {
    /// <summary>
    /// Модель параметра запроса на получения индекса УДК
    /// </summary>
    public class GetUdkByIdQuery : IRequest<UDK> {
        /// <summary>
        /// Уникальный идентификатор УДК
        /// </summary>
        public required Guid UdkId { get; set; }
    }
}