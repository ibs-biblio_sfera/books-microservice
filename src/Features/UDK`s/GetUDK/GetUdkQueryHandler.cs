using MediatR;
using BooksMicroservice.Models.UDKS;
using BooksMicroservice.Database;
using Microsoft.EntityFrameworkCore;

namespace BooksMicroservice.Features.UDKS {
    /// <summary>
    /// Моудль для получения списка УДК
    /// </summary>
    /// <param name="context">Контекст базы данных</param>
    public class GetUdkQueryHandler(ApplicationDbContext context) : IRequestHandler<GetUdkQuery, UdkListViewModel> {
        private readonly ApplicationDbContext _context = context;

        /// <summary>
        /// Метод для получения списка УДК
        /// </summary>
        /// <param name="request">Параметры запроса</param>
        /// <param name="cancellationToken"></param>
        /// <returns>Список индексов УДК</returns>
        public async Task<UdkListViewModel> Handle(GetUdkQuery request, CancellationToken cancellationToken) {
            var udks = await _context.UDKS.ToListAsync(cancellationToken);
            return new UdkListViewModel {
                UDKS = udks
            };
        }
    }
}