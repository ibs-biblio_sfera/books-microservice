using BooksMicroservice.Database;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace BooksMicroservice.Features.UDKS {
    public class GetUdkByIdHandler(ApplicationDbContext context, ConnectionMultiplexer redis) : IRequestHandler<GetUdkByIdQuery, UDK> {
        private readonly ApplicationDbContext _context = context;
        private readonly ConnectionMultiplexer _redis = redis;

        public async Task<UDK> Handle(GetUdkByIdQuery request, CancellationToken cancellationToken) {
            var cacheKey = $"udk:{request.UdkId}";
            var redisDb = _redis.GetDatabase();
            var cachedBbk = await redisDb.StringGetAsync(cacheKey);
            if (!cachedBbk.IsNullOrEmpty)
            {
                return JsonConvert.DeserializeObject<UDK>(cachedBbk);
            }
            var response = await _context.UDKS.FirstOrDefaultAsync(x => x.UdkId == request.UdkId, cancellationToken);
            if (response != null)
            {
                await redisDb.StringSetAsync(cacheKey, JsonConvert.SerializeObject(response), TimeSpan.FromMinutes(10));
            }
            return response;
        }
    }
}