using BooksMicroservice.Database;
using MediatR;
using StackExchange.Redis;

namespace BooksMicroservice.Features.UDKS {
    /// <summary>
    /// Класс удаления жанров
    /// </summary>
    /// <param name="context">Контекст базы данных</param>
    public class DeleteUdkHadler(ApplicationDbContext context, ConnectionMultiplexer redis) : IRequestHandler<DeleteUdkCommand, Unit> {
        private readonly ApplicationDbContext _context = context;
        private readonly IDatabase _redis = redis.GetDatabase();
        /// <summary>
        /// Метод находит УДК в базе и удаляет его
        /// </summary>
        /// <param name="request">Идентфикатор УДК</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="Exception">Вызывается если УДК не был найден</exception>
        public async Task<Unit> Handle(DeleteUdkCommand request, CancellationToken cancellationToken) {
            var cacheKey = $"udk:{request.UdkId}";
            var udk = await _context.UDKS.FindAsync(request.UdkId);
            if (udk == null) {
                throw new Exception($"ОШИБКА: Не удалось найти УДК с ID {request.UdkId}");
            }
            _context.UDKS.Remove(udk);
            await _context.SaveChangesAsync(cancellationToken);
            await _redis.KeyDeleteAsync(cacheKey);
            return Unit.Value;
        }
    }
}