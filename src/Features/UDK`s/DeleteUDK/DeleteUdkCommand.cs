using MediatR;

namespace BooksMicroservice.Features.UDKS {
    /// <summary>
    /// Модель данных для осуществления удаления УДК
    /// </summary>
    public class DeleteUdkCommand : IRequest<Unit> {
        /// <summary>
        /// Уникальный идентификатор УДК
        /// </summary>
        public Guid UdkId { get; set; }
    }
}