using BooksMicroservice.Database;
using MediatR;

namespace BooksMicroservice.Features.Genres {
    /// <summary>
    /// Класс удаления жанров
    /// </summary>
    /// <param name="context">Контекст базы данных</param>
    public class DeleteGenreHadler(ApplicationDbContext context) : IRequestHandler<DeleteGenreCommand, Unit> {
        private readonly ApplicationDbContext _context = context;

        public async Task<Unit> Handle(DeleteGenreCommand request, CancellationToken cancellationToken) {
            var genre = await _context.Genres.FindAsync(request.GenreId);
            if (genre == null) {
                throw new Exception($"ОШИБКА: Не удалось найти жанр с ID {request.GenreId}");
            }
            _context.Genres.Remove(genre);
            _ = await _context.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}