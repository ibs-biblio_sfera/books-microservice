using MediatR;

namespace BooksMicroservice.Features.Genres {
    /// <summary>
    /// Модель данных для осуществления удаления жанра
    /// </summary>
    public class DeleteGenreCommand : IRequest<Unit> {
        /// <summary>
        /// Уникальный идентификатор жанра
        /// </summary>
        public Guid GenreId { get; set; }
    }
}