using MediatR;
using BooksMicroservice.Models.Genres;

namespace BooksMicroservice.Features.Genres {
    public class GetGenresQuery : IRequest<GenresListViewModel> {
        public int Page { get; set; }
    }
}